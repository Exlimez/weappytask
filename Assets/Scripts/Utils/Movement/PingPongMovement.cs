﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PingPongMovement //move from one point to another and back
{
    public static IEnumerator Move(Transform objectTransform, PingPongMovementVariables vars, Vector3 startPathPoint, Vector3 endPathPoint,
        float speed = 1.0f, bool pauseAtStartPointOnly = false, Action onStart = null,
        Action onEnd = null)
    {

        if(vars.destinationSwitched)
        {
            vars.startTime = Time.time;
            vars.destinationSwitched = false;
        }

        float passedTime = Time.time - vars.startTime;

        if (Math.Abs(passedTime - Time.deltaTime) < 0.01)
        {
            onStart?.Invoke();
        }

        float distanceCovered = passedTime * speed;
        float fractionOfPath = distanceCovered * vars.inversePathLength;

        

        objectTransform.position = !vars.backDestination ?
            Vector3.Lerp(startPathPoint, endPathPoint, fractionOfPath)
            : Vector3.Lerp(endPathPoint, startPathPoint, fractionOfPath);

        if (fractionOfPath > 0.99f)
        {
            vars.backDestination = !vars.backDestination;
            vars.destinationSwitched = true;

            if (!vars.backDestination)
            {
                onEnd?.Invoke();
            }

            if (!(pauseAtStartPointOnly && vars.backDestination)) // if at the secondPathPoint and shouldn't pause here
            {
                yield return new WaitForSeconds(vars.pauseBetweenPoints);
            }
            
        }
        yield return null;
    }
}

public class PingPongMovementVariables //for changing by reference
{
    public float pauseBetweenPoints = 2.0f;
    public float startTime;
    public float inversePathLength; // 1 / (distance between path points)
    public bool backDestination = false;
    public bool destinationSwitched = false;
}

