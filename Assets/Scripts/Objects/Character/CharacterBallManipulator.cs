﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterMovement))]
public class CharacterBallManipulator : MonoBehaviour
{
    [SerializeField] private Transform placeForPickedObject;
    [SerializeField] private LayerMask pickableLayer;
    [SerializeField] private float pickUpRadius = 0.6f;
    private CharacterMovement characterMovementComp;
    private GameObject pickedObject;

    private void Start()
    {
        characterMovementComp = GetComponent<CharacterMovement>();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            if(pickedObject != null)
            {
                Throw();
            }
            else
            {
                TryPickUp();
            }
        }
    }

    private void Throw()
    {
        pickedObject.GetComponent<BallController>().Push();
        pickedObject = null;
    }

    private void TryPickUp()
    {
        Collider2D itemCollider = Physics2D.OverlapCircle(transform.position, pickUpRadius, pickableLayer);

        if(itemCollider != null)
        {
            pickedObject = itemCollider.gameObject;
            PickUp();
        }
    }

    private void PickUp()
    {
        characterMovementComp.AllowMovement(false);
        pickedObject.transform.SetParent(transform);
        pickedObject.transform.DOJump(placeForPickedObject.position, 1, 1, 0.5f)
            .OnComplete(() => characterMovementComp.AllowMovement(true));
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireSphere(transform.position, pickUpRadius);
    }
}
