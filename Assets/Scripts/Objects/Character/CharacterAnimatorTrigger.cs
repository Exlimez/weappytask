﻿
///<summary>
///<br>1. Cast to int when call</br>
///<br>2. To get animator parameter id, use Animator.StringToHash</br>
///</summary>
public enum CharacterAnimatorTrigger : int
{
    Walk = 765711723,
    Jump = 125937960,
    Sit = 1928080432,
    Idle = 2081823275
}
