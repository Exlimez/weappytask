﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterHealth : MonoBehaviour
{
    [SerializeField] private int hp = 3;
    [SerializeField] private GameObject[] hpSprites;

    public void GetDamage()
    {
        hp--;
        RenderHPBar();

        if (hp <= 0)
        {
            Die();
        }
    }

    private void RenderHPBar()
    {
        hpSprites[hp].SetActive(false);
    }

    private void Die()
    {
        gameObject.SetActive(false);
    }
}
