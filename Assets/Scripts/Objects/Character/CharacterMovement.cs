﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class CharacterMovement : MonoBehaviour
{
    [Header("Jump")]
    [SerializeField] private float jumpForce = 1.0f;
    private bool inJump = false;
    

    [Header("Ground Check")]
    [SerializeField] private LayerMask groundLayerCheck;
    [SerializeField] private Transform groundCheckPosition;
    private const float groundRadius = .4f;
    private bool onGround = false;

    [Header("Horizontal Move")]
    [SerializeField] private float horizontalSpeed;
    [SerializeField] [Range(0, .3f)] private float movementSmoothing = .05f;
    private Vector3 characterVelocity = Vector3.zero;
    private float horizontalMove = 0.0f;
    private bool facingLeft = false;
    private bool inWalk = false;
    private bool inIdle = false;
    private bool allowMovement = true;

    [Header("Character Components")]
    [SerializeField] private Animator animator;
    [SerializeField] private Rigidbody2D rigidBody;

    void Update()
    {
        horizontalMove = Input.GetAxisRaw("Horizontal");
    }

    void FixedUpdate()
    {
        if(allowMovement)
        {
            int obstaclesCount = Physics2D.OverlapCircleAll(groundCheckPosition.position, groundRadius, groundLayerCheck).Length;
            if (obstaclesCount > 0)
            {
                onGround = true;
                inJump = false;
            }

            if (Input.GetButton("Jump"))
            {
                if (onGround)
                {
                    rigidBody.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
                    animator.SetTrigger((int)CharacterAnimatorTrigger.Jump);
                    onGround = false;
                }
                if (inJump == false)
                {
                    inJump = true;
                    inWalk = false;
                    inIdle = false;
                }
            }

            Vector3 targetVelocity = new Vector2(horizontalMove * horizontalSpeed,
                obstaclesCount > 0 ? rigidBody.velocity.y / obstaclesCount : rigidBody.velocity.y);

            rigidBody.velocity = Vector3.SmoothDamp(rigidBody.velocity, targetVelocity,
                ref characterVelocity, movementSmoothing);


            if (horizontalMove != 0 && inWalk == false && inJump == false)
            {
                animator.SetTrigger((int)CharacterAnimatorTrigger.Walk);
                inWalk = true;
                inIdle = false;
            }

            if (horizontalMove == 0 && inIdle == false && inJump == false)
            {
                animator.SetTrigger((int)CharacterAnimatorTrigger.Idle);
                inIdle = true;
                inWalk = false;
            }

            if (horizontalMove > 0 && facingLeft)
            {
                Flip();
            }

            if (horizontalMove < 0 && !facingLeft)
            {
                Flip();
            }
        }
    }

    private void Flip()
    {
        facingLeft = !facingLeft;

        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    public void AllowMovement(bool allowMovement)
    {
        this.allowMovement = allowMovement;
    }
}
