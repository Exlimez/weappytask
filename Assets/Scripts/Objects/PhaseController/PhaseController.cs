﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhaseController : MonoBehaviour
{
    [SerializeField] private EnemyHealth enemyHealthComp;
    [SerializeField] private OwlMovement owlMovementComp;

    private void Start()
    {
        enemyHealthComp.onHPThresholdReached = owlMovementComp.SetReadyToPull;
    }

}
