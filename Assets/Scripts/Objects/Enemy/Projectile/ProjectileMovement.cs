﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileMovement : MonoBehaviour
{
    [SerializeField] private float speed = 0.0001f;
    private PingPongMovementVariables horizontalMoveVariables;

    public void StartMovement(TweenCallback onMoveEnd)
    {
        transform.DOMoveY(transform.position.y - 8, 3).SetEase(Ease.InSine)
            .OnComplete(onMoveEnd);

        horizontalMoveVariables = new PingPongMovementVariables()
        {
            startTime = Time.time,
            inversePathLength = 1 / Vector3.Distance(transform.position,
            transform.position + new Vector3(0, -5, 0))
        };
    }
}
