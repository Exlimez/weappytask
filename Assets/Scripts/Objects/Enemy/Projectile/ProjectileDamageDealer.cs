﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class ProjectileDamageDealer : MonoBehaviour
{
    [SerializeField] private float damageRadius;
    [SerializeField] private LayerMask playerLayer;
    private bool readyDealDamage = true;

    private void Update()
    {
        if(isActiveAndEnabled && readyDealDamage)
        {
            var playerCollider = Physics2D.OverlapCircle(transform.position, damageRadius, playerLayer);
            if (playerCollider)
            {
                playerCollider.gameObject.GetComponentInParent<CharacterHealth>().GetDamage();
                readyDealDamage = false;
                Task.Delay(1000).ContinueWith(t => Reload());
            }
        }
    }

    private void Reload()
    {
        readyDealDamage = true;
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireSphere(transform.position, damageRadius);
    }
}
