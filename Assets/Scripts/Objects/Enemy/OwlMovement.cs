﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class OwlMovement : MonoBehaviour
{
    [SerializeField] private Transform startPathPoint;
    [SerializeField] private Transform endPathPoint;
    [SerializeField] private Vector3 pullPathPoint;
    [SerializeField] private float horizontalSpeed = 2f;
    [SerializeField] private float verticalSpeed = 5f;
    private PingPongMovementVariables horizontalMoveVars;
    private PingPongMovementVariables verticalMoveVars;
    private bool shouldPull = false;
    private bool horizontalMovePaused = false;

    private void Start()
    { 
        StartCoroutine(StartHorizontalMove());
        StartCoroutine(ListenForStartPull());
    }

    private IEnumerator StartHorizontalMove()
    {
        horizontalMoveVars = new PingPongMovementVariables()
        {
            pauseBetweenPoints = 1.0f,
            startTime = Time.time,
            inversePathLength = 1 / Vector3.Distance(startPathPoint.position, endPathPoint.position)
        };

        while (true)
        {
            while(horizontalMovePaused)
            {
                yield return new WaitForSeconds(0.05f);
            }

            yield return PingPongMovement.Move(transform, horizontalMoveVars,
                new Vector3(startPathPoint.position.x, transform.position.y),
                new Vector3(endPathPoint.position.x, transform.position.y),
                horizontalSpeed);
        }
    }

    private IEnumerator ListenForStartPull()
    {
        WaitForSeconds waitInterval = new WaitForSeconds(0.1f);
        while(!shouldPull)
        {
            yield return waitInterval;
        }

        verticalMoveVars = new PingPongMovementVariables()
        {
            pauseBetweenPoints = 3.0f,
            startTime = Time.time,
            inversePathLength = 1 / Vector3.Distance(transform.position,
            new Vector3(transform.position.x, startPathPoint.position.y - 5))
        };

        StartCoroutine(StartPull());
    }

    private IEnumerator StartPull()
    {
        while (true)
        {
            yield return PingPongMovement.Move(transform, verticalMoveVars,
                new Vector3(transform.position.x, startPathPoint.position.y),
                new Vector3(transform.position.x, startPathPoint.position.y - 5),
                verticalSpeed, true, () => horizontalMovePaused = true,
                () => horizontalMovePaused = false);
        }
    }

    public void SetReadyToPull()
    {
        shouldPull = true;
    }
}
