﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour
{
    [HideInInspector] public Action onHPThresholdReached;
    [SerializeField] private SpriteRenderer renderer;
    [SerializeField] private int hp = 4;
    [SerializeField] private int hpThreshold = 2;

    public void GetDamage()
    {
        DOTween.Sequence()
            .Append(renderer.DOFade(0, 0.15f))
            .Append(renderer.DOFade(1, 0.15f))
            .Append(renderer.DOFade(0, 0.15f))
            .Append(renderer.DOFade(1, 0.15f));
        hp--;

        if(hp == hpThreshold)
        {
            onHPThresholdReached();
        }

        if(hp <= 0)
        {
            Die();
        }
    }

    private void Die()
    {
        gameObject.SetActive(false);
    }
}
