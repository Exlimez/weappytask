﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileSpawner : MonoBehaviour
{
    [SerializeField] private GameObject projectilePrefab;
    [SerializeField] private Transform projectileSpawnPoint;
    [SerializeField] private float shootInterval = 2.0f;
    [SerializeField] private int projectilePoolSize = 10;
    private List<GameObject> projectilePool;
    private List<GameObject>.Enumerator projectilePoolEnumerator;
    private bool isDead = false;

    private void Awake()
    {
        InitializeProjectilePool();
        StartCoroutine(StartShooting());
    }

    private void InitializeProjectilePool()
    {
        projectilePool = new List<GameObject>();
        for (int i = 0; i < projectilePoolSize; i++)
        {
            GameObject obj = Instantiate(projectilePrefab, projectileSpawnPoint);
            obj.SetActive(false);
            projectilePool.Add(obj);
        }
        projectilePoolEnumerator = projectilePool.GetEnumerator();
    }

    private IEnumerator StartShooting()
    {
        while(!isDead)
        {
            yield return new WaitForSeconds(shootInterval);
            SpawnProjectile();
        }
    }

    private void SpawnProjectile()
    {
        if (!projectilePoolEnumerator.MoveNext())
        {
            ResetEnumerator(ref projectilePoolEnumerator);
            projectilePoolEnumerator.MoveNext();
        }

        projectilePoolEnumerator.Current.transform.SetParent(null);
        projectilePoolEnumerator.Current.SetActive(true);

        GameObject currentProjectile = projectilePoolEnumerator.Current;

        projectilePoolEnumerator.Current.GetComponent<ProjectileMovement>()
            .StartMovement(() =>
            {
                currentProjectile.SetActive(false);
                currentProjectile.transform.position = projectileSpawnPoint.position;
                currentProjectile.transform.SetParent(projectileSpawnPoint);
            });
    }

    void ResetEnumerator<T>(ref T enumerator) where T : IEnumerator
    {
        enumerator.Reset();
    }
}
