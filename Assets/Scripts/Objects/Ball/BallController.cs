﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class BallController : MonoBehaviour
{
    [SerializeField] private LayerMask obstacleLayer;
    [SerializeField] private float ballRadius = 1.0f;
    private bool gonnaDealDamage = false;
    
    void Update()
    {
        if(gonnaDealDamage)
        {
            var obstacleCollider = Physics2D.OverlapCircle(transform.position, ballRadius, obstacleLayer);
            if(obstacleCollider != null)
            {
                if (obstacleCollider.gameObject.layer == 10) //if enemy
                {
                    DealDamage(obstacleCollider.gameObject);
                    gonnaDealDamage = false;
                }
                if (obstacleCollider.gameObject.layer == 8) //if ground
                {
                    gonnaDealDamage = false;
                }
            }
        }
    }

    private void DealDamage(GameObject enemy)
    {
        enemy.GetComponent<EnemyHealth>().GetDamage();
    }

    public void Push()
    {
        gonnaDealDamage = true;
        transform.SetParent(null);
        DOTween.Sequence()
            .Append(transform.DOMoveY(3, 0.5f).SetEase(Ease.OutQuart))
            .Append(transform.DOMoveY(-2.6f, 0.5f).SetEase(Ease.OutBounce));
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireSphere(transform.position, ballRadius);
    }
}
